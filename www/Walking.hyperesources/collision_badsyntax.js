$.collision = function(selector) {
 
  var data = [];
 
  $(selector).each(function(){
    
    var elem    = $(this);
    var offset  = elem.offset();
    var width   = elem.outerWidth();
    var height  = elem.outerHeight();
 
    data.push({
      tl: { x: offset.left, y: offset.top },
      tr: { x: offset.left + width, y: offset.top },
      bl: { x: offset.left, y: offset.top + height },
      br: { x: offset.left + width, y: offset.top + height }
    });
  });
 
  var i, l;
 
  i = data.length;
  while(i--) {
    l = data.length;
    while(l-- && l !== i) {
      if (!( 
        data[l].br.x < data[i].bl.x || 
        data[l].bl.x > data[i].br.x || 
        data[l].bl.y < data[i].tl.y || 
        data[l].tl.y > data[i].bl.y 
      )) {
        return true;
      }
    }
  }   
  return false;
};